## POTI Gameplay Config Changelog

#### Version 2.0

* Rewrote how the mod works:
  * Changes are now only applied once, when you start a new game
  * To re-apply changes at any time, navigate to the script settings menu (ESC >> Options >> Scripts >> MOMW Gameplay) and set "Factory Reset" to "yes"

<!-- [Download Link](https://gitlab.com/modding-openmw/poti-gameplay-config/-/packages/#TODO) -->

#### Version 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/poti-gameplay-config/-/packages/17469202)
