local async = require("openmw.async")
local core = require('openmw.core')
local storage = require('openmw.storage')
local I = require("openmw.interfaces")

local MOD_ID = "POTI_Gameplay_Config"
local SETTINGS_KEY = 'Settings' .. MOD_ID

local isOpenMW049 = core.API_REVISION > 29

I.Settings.registerPage {
    key = MOD_ID,
    l10n = MOD_ID,
    name = "name",
    description = "description"
}

I.Settings.registerGroup {
    key = SETTINGS_KEY,
    page = MOD_ID,
    l10n = MOD_ID,
    name = "settingsTitle",
    permanentStorage = false,
    settings = {
        {
            key = 'factoryReset',
            name = "factoryReset_name",
            description = "factoryReset_desc",
            default = false,
            renderer = 'checkbox'
        }
    }
}

local function checkAndSet(data)
    if data.contentFile == nil then
        print("Missing required contentFile")
    end
    if data.key == nil then
        print("Missing required key")
        print(data.contentFile)
    end
    if data.value == nil then
        print("Missing required value")
        print(data.contentFile)
        print(data.key)
    end
    if data.section == nil then
        print("Missing required section")
        print(data.contentFile)
        print(data.key)
        print(data.value)
    end
    if isOpenMW049 and core.contentFiles.has(data.contentFile) then
        print(string.format("Setting \"%s=%s\" for \"%s\" (%s)", data.key, data.value, data.section, data.contentFile))
        storage.playerSection(data.section):set(data.key, data.value)
    elseif not isOpenMW049 then
        print(string.format("Setting \"%s=%s\" for \"%s\" (%s)", data.key, data.value, data.section, data.contentFile))
        storage.playerSection(data.section):set(data.key, data.value)
    end
end

local function applyAll()
    -- Sol Bodz Stat Shaders
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolBodzStatShaders",
            key = "healthIntensity",
            value = 0.6
    })
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolBodzStatShaders",
            key = "magicIntensity",
            value = 0.6
    })

    -- Sol Charge Attack Parry
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolChargeAttackParry",
            key = "verbose",
            value = 0
    })

    -- Sol Sneak Jump Dodge
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolSneakJumpDodge",
            key = "verbose",
            value = false
    })

    -- Sol Staff Spell Buffs
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_sol_StaffSpellBuff",
            key = "verbose",
            value = false
    })

    -- Sol Swim Boost Drain
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolSwimBoostDrain",
            key = "verbose",
            value = false
    })

    -- Sol Timed Dir Attacks
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolTimedDirAttacks",
            key = "verbose",
            value = 0
    })

    -- Sol Weighty Charge Attacks
    checkAndSet({
            contentFile = "SolCombatPack.omwscripts",
            section = "Settings_SolWeightyChargeAttacks",
            key = "verbose",
            value = 0
    })

    print("POTI Gameplay Config: player settings have been setup")
end

local function factoryReset(_, key)
    if key == "factoryReset" and storage.playerSection(SETTINGS_KEY):get("factoryReset") == true then
        applyAll()
        print("POTI Gameplay Config: factory reset executed")
    end
end
storage.playerSection(SETTINGS_KEY):subscribe(async:callback(factoryReset))

return {engineHandlers = {onInit = applyAll}}
