# POTI Gameplay Config

Automatically configure various mods.

**Requires OpenMW 0.48 or newer!**

This mod can be disabled to stop the changes or re-enabled to resume them at any time.

#### Web

[Project Home](https://modding-openmw.gitlab.io/poti-gameplay-config/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/poti-gameplay-config)

#### Installation

**OpenMW 0.48 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/poti-gameplay-config/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Settings\poti-gameplay-config

        # Linux
        /home/username/games/OpenMWMods/Settings/poti-gameplay-config

        # macOS
        /Users/username/games/OpenMWMods/Settings/poti-gameplay-config

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Settings\poti-gameplay-config"`)
1. Add `content=poti-gameplay-config.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher
1. Each time you run OpenMW, the following will be set if the related mod is active:
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Charge Attack Parry](https://www.nexusmods.com/morrowind/mods/52221)): **0**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Sneak Jump Dodge](https://www.nexusmods.com/morrowind/mods/52221)): **No**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Staff Spell Buffs](https://www.nexusmods.com/morrowind/mods/52221)): **No**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Swim Boost Drain](https://www.nexusmods.com/morrowind/mods/52221)): **No**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Timed Directional Attacks](https://www.nexusmods.com/morrowind/mods/52221)): **No**
    * Verbose ([Solthas Combat Pack (OpenMW Lua): Weighty Charge Attacks](https://www.nexusmods.com/morrowind/mods/52221)): **0**

In order to customize these settings you must disable this mod (it is safe to disable or re-enable at any time mid-save).

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/poti-gameplay-config/-/issues)
* Email `poti-gameplay-config@modding-openmw.com`
* Contact the author on Discord: `johnnyhostile#6749`
* Contact the author on Libera.chat IRC: `johnnyhostile`
